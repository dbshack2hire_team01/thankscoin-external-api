package com.dbshac2hire.thankcoin.external.api.controller;

import com.dbshac2hire.thankcoin.external.api.dto.TransferDto;
import com.dbshac2hire.thankcoin.external.api.service.TransferService;
import com.dbshac2hire.thankcoin.external.api.vo.LoginVo;
import com.dbshac2hire.thankcoin.external.api.vo.TransferResultVo;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author isak.rabin
 */
@RestController
public class TransactionController {

    @Autowired
    private TransferService transferService;

    /**
     *
     * @param transferDto
     * @return
     */
    @RequestMapping(path = "/send", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<TransferResultVo> send(HttpServletRequest request, @RequestBody TransferDto transferDto) {
        String token = request.getHeader("Authorization");

        TransferResultVo result = transferService.send(token, transferDto.getAccountFrom(), transferDto.getEmailTo(), transferDto.getAmount(), transferDto.getDescription(), transferDto.getSignature());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     *
     * @param transferDto
     * @return
     */
    @RequestMapping(path = "/receive", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<TransferResultVo> receive(@RequestBody TransferDto transferDto) {
        TransferResultVo result = transferService.receieve(transferDto.getAccountFrom(), transferDto.getAccountTo(), transferDto.getAmount(), transferDto.getDescription(), transferDto.getSignature());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}

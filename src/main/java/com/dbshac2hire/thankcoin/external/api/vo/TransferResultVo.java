package com.dbshac2hire.thankcoin.external.api.vo;

import java.util.Date;

/**
 *
 * @author isak.rabin
 */
public class TransferResultVo extends BaseVo {

    private boolean open;
    private Long balance;
    private Date lastTransaction;
    private Date created;
    private String number;

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Date getLastTransaction() {
        return lastTransaction;
    }

    public void setLastTransaction(Date lastTransaction) {
        this.lastTransaction = lastTransaction;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}

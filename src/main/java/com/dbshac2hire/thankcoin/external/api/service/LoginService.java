package com.dbshac2hire.thankcoin.external.api.service;

import com.dbshac2hire.thankcoin.external.api.dto.LoginDto;
import com.dbshac2hire.thankcoin.external.api.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author isak.rabin
 */
@Service
public class LoginService {

    private final String WALLET_ENDPOINT = "http://localhost:8090/";

    @Autowired
    private RestClientService restClient;

    public LoginVo login(String email, String password) {
        LoginDto loginDto = new LoginDto();
        loginDto.setEmail(email);
        loginDto.setPassword(password);

        String loginURL = WALLET_ENDPOINT + "/users/login/";
        ResponseEntity<LoginVo> result = restClient.getRestTemplate().postForEntity(loginURL, loginDto, LoginVo.class);
        
        return result.getBody();
    }

}
